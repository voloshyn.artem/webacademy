# WebAcademy



## This repo is for Git practise (solution for below tasks)


# Task 1


Create a remote repository on one of the platforms of your choice:
– GitLab
- GitHub
– Bitbucket

    1. Configure the repository on the local machine
    2. Create branches main, development
    3. Make a feature branch, put any text file there and make a series  of commits
    4. Commit changes to the main branch
    5. Create multiple tags

Complete the task in the form of a link to your public repository.
Repository history should be available. Post the link in your personal account.
If the repository is private, add 1-2 print screens where you can see the progress (commits, branches).

# Task 2
Create a file in a feature branch that contains a markdown file with the chapter “code-review”
https://github.blog/2022-06-30-write-better-commits-build-better-projects/#code-review
Create the pull request in the main branch

